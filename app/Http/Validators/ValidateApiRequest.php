<?php

namespace App\Http\Validators;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ValidateApiRequest
{

    private $errorJsonResponse;
    private $correctJsonInput;

    public function __construct()
    {

    }

    public function getErrorJsonResponse() {

        return $this->errorJsonResponse;
    }

    public function getCorrectJsonInput() {
        return $this->correctJsonInput;
    }


    public function addLocationCorrect(Request $request): bool {

        $validateJsonInput = new ValidateJsonInput();

        if(!$validateJsonInput->properJsonStructure()) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Bad json structure",
                    'return_data' => null
                ]
            ]);

            return false;
        }

        if(!$validateJsonInput->properJsonRequest($request)) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Content-Type needs to be application/json and use POST method",
                    'return_data' => null
                ]
            ]);

            return false;
        }


        foreach ($request->all() as $el) {

            $validator = Validator::make($el, [
                'name' => 'required',
                'lat' => 'required|numeric',
                'lng' => 'required|numeric'
            ],[],[
                'name' => 'name',
                'lat' => 'lat',
                'lng' => 'lng'
            ]);

            if ($validator->fails()) {

                $errors = $validator->errors();

                if ($errors->has('name')) {
                    $this->errorJsonResponse = response()->json([
                        'callback' => [
                            'status' => false,
                            'error_message' => $errors->first('name'),
                            'return_data' => null
                        ]
                    ]);
                }

                if ($errors->has('lat')) {
                    $this->errorJsonResponse = response()->json([
                        'callback' => [
                            'status' => false,
                            'error_message' => $errors->first('lat'),
                            'return_data' => null
                        ]
                    ]);
                }

                if ($errors->has('lng')) {
                    $this->errorJsonResponse = response()->json([
                        'callback' => [
                            'status' => false,
                            'error_message' => $errors->first('lng'),
                            'return_data' => null
                        ]
                    ]);
                }

                return false;
            }
        }

        $this->correctJsonInput = $request->all();
        return true;
    }

    public function nearestPointCorrect(Request $request): bool {

        $validateJsonInput = new ValidateJsonInput();

        if(!$validateJsonInput->properJsonStructure()) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Bad json structure",
                    'return_data' => null
                ]
            ]);

            return false;
        }

        if(!$validateJsonInput->properJsonRequest($request)) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Content-Type needs to be application/json and use POST method",
                    'return_data' => null
                ]
            ]);

            return false;
        }

        $jsonInput = $request->only(
            'lat',
            'lng'
        );

        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ],[],[
            'lat' => 'lat',
            'lng' => 'lng'
        ]);


        if ($validator->fails()) {

            $errors = $validator->errors();

            if ($errors->has('lat')) {
                $this->errorJsonResponse = response()->json([
                    'callback' => [
                        'status' => false,
                        'error_message' => $errors->first('lat'),
                        'return_data' => null
                    ]
                ]);
            }

            if ($errors->has('lng')) {
                $this->errorJsonResponse = response()->json([
                    'callback' => [
                        'status' => false,
                        'error_message' => $errors->first('lng'),
                        'return_data' => null
                    ]
                ]);
            }

            return false;
        }

        $this->correctJsonInput = $jsonInput;
        return true;




    }

    public function thresholdPointRequestCorrect(Request $request): bool {


        $validateJsonInput = new ValidateJsonInput();

        if(!$validateJsonInput->properJsonStructure()) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Bad json structure",
                    'return_data' => null
                ]
            ]);

            return false;
        }

        if(!$validateJsonInput->properJsonRequest($request)) {
            $this->errorJsonResponse = response()->json([
                'callback' => [
                    'status' => false,
                    'error_message' => "Content-Type needs to be application/json and use POST method",
                    'return_data' => null
                ]
            ]);

            return false;
        }


        $jsonInput = $request->only(
            'lat',
            'lng',
            'threshold'
        );

        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'threshold' => 'required|numeric',
        ],[],[
            'lat' => 'lat',
            'lng' => 'lng',
            'threshold' => 'threshold',
        ]);



        if ($validator->fails()) {

            $errors = $validator->errors();

            if ($errors->has('lat')) {
                $this->errorJsonResponse = response()->json([
                    'callback' => [
                        'status' => false,
                        'error_message' => $errors->first('lat'),
                        'return_data' => null
                    ]
                ]);
            }

            if ($errors->has('lng')) {
                $this->errorJsonResponse = response()->json([
                    'callback' => [
                        'status' => false,
                        'error_message' => $errors->first('lng'),
                        'return_data' => null
                    ]
                ]);
            }

            if ($errors->has('threshold')) {
                $this->errorJsonResponse = response()->json([
                    'callback' => [
                        'status' => false,
                        'error_message' => $errors->first('threshold'),
                        'return_data' => null
                    ]
                ]);
            }

            return false;
        }

        $this->correctJsonInput = $jsonInput;
        return true;

    }


}