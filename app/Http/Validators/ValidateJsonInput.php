<?php

namespace App\Http\Validators;


use Illuminate\Http\Request;

class ValidateJsonInput
{

    public function __construct()
    {

    }

    public function properJsonStructure(): bool
    {

        if (json_last_error() === JSON_ERROR_NONE) {
            return true;
        } else {
            return false;
        }
    }

    public function properJsonRequest(Request $request): bool
    {

        if ($request->isJson() ||
            $request->wantsJson() ||
            $request->ajax() &&
            $request->isMethod('post')) {

            return true;
        }

        return false;
    }
}