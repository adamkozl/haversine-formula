<?php

namespace App\Http\Helpers;

class CalculateDistanceBetweenTwoPoints {

    private $distance;

    public function __construct(float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo) {

        $this->setDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo);
    }

    private function setDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {

        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $distanceInKm = ($miles * 1.609344);

        $this->distance = $distanceInKm;

    }

    public function getDistance(): float {

        return $this->distance;
    }


}