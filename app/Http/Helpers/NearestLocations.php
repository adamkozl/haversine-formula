<?php

namespace App\Http\Helpers;

use App\Location;

class NearestLocations
{

    private $latitudeFrom;
    private $longitudeFrom;

    public function __construct(float $latitudeFrom, float $longitudeFrom)
    {

        $this->setLatitudeFrom($latitudeFrom);
        $this->setLongitudeFrom($longitudeFrom);

    }

    private function setLatitudeFrom($latitudeFrom)
    {

        $this->latitudeFrom = $latitudeFrom;
    }

    private function setLongitudeFrom($longitudeFrom)
    {

        $this->longitudeFrom = $longitudeFrom;
    }


    public function getTheNearestPoint(): array
    {

        $array = [];

        foreach (Location::all() as $location) {

            $calculateDistanceBetweenTwoPoints = new CalculateDistanceBetweenTwoPoints($this->latitudeFrom, $this->longitudeFrom, $location->lat, $location->lng);
            array_push($array, ['location_name' => $location->name, 'distance' => $calculateDistanceBetweenTwoPoints->getDistance()]);
        }

        array_multisort(array_column($array, 'distance'), SORT_ASC, $array);

        return reset($array);
    }

    public function getTheNearestPointsWithThreshold(float $threshold): array
    {

        $array = [];

        foreach (Location::all() as $location) {

            $calculateDistanceBetweenTwoPoints = new CalculateDistanceBetweenTwoPoints($this->latitudeFrom, $this->longitudeFrom, $location->lat, $location->lng);
            if ($calculateDistanceBetweenTwoPoints->getDistance() <= $threshold) {
                array_push($array, ['location_name' => $location->name, 'distance' => $calculateDistanceBetweenTwoPoints->getDistance()]);
            }

        }

        return $array;
    }


}