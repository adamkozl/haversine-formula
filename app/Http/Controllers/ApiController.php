<?php

namespace App\Http\Controllers;

use App\Http\Helpers\NearestLocations;
use App\Http\Validators\ValidateApiRequest;
use App\Location;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function __construct()
    {

    }

    public function postAddLocation(Request $request): JsonResponse
    {

        $validateApiRequest = new ValidateApiRequest();
        if(!$validateApiRequest->addLocationCorrect($request)) {
            return $validateApiRequest->getErrorJsonResponse();
        }


        foreach ($validateApiRequest->getCorrectJsonInput() as $el) {
            Location::create([
                'name' => $el['name'],
                'lat' => $el['lat'],
                'lng' => $el['lng']
            ]);
        }

        return response()->json([
            'callback' => [
                'status' => true,
                'return_data' => $request->all()
            ]
        ]);
    }

    public function postNearestPoint(Request $request): JsonResponse
    {

        $validateApiRequest = new ValidateApiRequest();
        if(!$validateApiRequest->nearestPointCorrect($request)) {
            return $validateApiRequest->getErrorJsonResponse();
        }

        $nearestLocations = new NearestLocations($validateApiRequest->getCorrectJsonInput()['lat'], $validateApiRequest->getCorrectJsonInput()['lng']);

        return response()->json([
            'callback' => [
                'status' => true,
                'return_data' => $nearestLocations->getTheNearestPoint()
            ]
        ]);
    }

    public function postThresholdPoint(Request $request): JsonResponse
    {


        $validateApiRequest = new ValidateApiRequest();
        if(!$validateApiRequest->thresholdPointRequestCorrect($request)) {
            return $validateApiRequest->getErrorJsonResponse();
        }

        $nearestLocations = new NearestLocations($validateApiRequest->getCorrectJsonInput()['lat'], $validateApiRequest->getCorrectJsonInput()['lng']);

        return response()->json([
            'callback' => [
                'status' => true,
                'return_data' => $nearestLocations->getTheNearestPointsWithThreshold($validateApiRequest->getCorrectJsonInput()['threshold'])
            ]
        ]);
    }
}
