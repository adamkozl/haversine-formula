This is simple REST API project based on PHP Laravel 5.8 framework.

In project you can see how Haversine Formula works.

Requirements: PHP 7.1 or larger and Composer

1. Clone project:
git clone 
2. Go to project file and install dependencies: 
composer install
3. In project copy ".env.example" and name it ".env"
4. In ".env" set database connection: 

DB_DATABASE=your_local_db_name

DB_USERNAME=your_user_name    

DB_PASSWORD=your_pass
5. Set Laravel application key by command:
php artisan key:generate
6. Clear application cache:
php artisan config:cache
7. Create database structure:
php artisan migrate
8. Fire virtual server by command:
php artisan serve
9. Project is ready to use


- adding locations:
localhost:8000/api/add-location
JSON PAYLOAD EXAMPLE

[  
   {  
      "name":"Lokalizacja 1",
      "lat":21.132312,
      "lng":21.132312
   },
   
   {  
      "name":"Lokalizacja 2",
      "lat":42.03452,
      "lng":31.3128
   }
   
]

- finding the nearest point:
localhost:8000/api/nearest-point

JSON PAYLOAD EXAMPLE

{
	"name": "Imię użytkownika",
	"lat": 8.2312,
	"lng": 12.723
}

- finding the nearest point with threshold (in km):

localhost:8000/api/threshold-point

JSON PAYLOAD EXAMPLE:

{
	"name": "Imię użytkownika",
	"lat": 8.2312,
	"lng": 12.723,
	"threshold": 3000
}


PROJECT STRCTURE:
- ROUTES: /routes/api.php
- CONTROLLERS: /app/Http/Controllers/ApiController.php
- VALIDATION LOGIC: /app/Http/Validators
- HELPERS: /app/Http/Helpers
- DB MIGRATIONS: /database/migrations


Developer: Adam Kozlowski / adam@coolcode.pl
Last update: 18.04.2019
