<?php


Route::post('/add-location', [
    'as' => 'post.add-location',
    'uses' => 'ApiController@postAddLocation',
]);

Route::post('/nearest-point', [
    'as' => 'post.nearest-point',
    'uses' => 'ApiController@postNearestPoint',
]);

Route::post('/threshold-point', [
    'as' => 'post.threshold-point',
    'uses' => 'ApiController@postThresholdPoint',
]);
